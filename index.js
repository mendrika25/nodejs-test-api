const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const userRoute = require('./routes/user.route');
const conversatinRoute = require('./routes/conversation.route');
const { launchScript } = require('./db-scripts/user.faker');

dotenv.config();

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true}, () => {
    console.log('connected to the database');
})

app.use(express.json());
app.use(cors());
app.use('/api/user', userRoute);
app.use('/api/conversation', conversatinRoute);

const port = process.env.APP_PORT;

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
});

launchScript();