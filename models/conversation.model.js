const mongoose = require('mongoose');
const User = require('./user.model');

const messageSchema = new mongoose.Schema({
    from: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    message: {type: String}
});

const conversationSchema = new mongoose.Schema({
    name: {type: String},
    messages: [{type: messageSchema, default: []}],
    createdBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    members: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    closed: {type: Boolean, default: false}
});

module.exports = mongoose.model('Conversation', conversationSchema);