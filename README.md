# nodejs-test-api

1- Make sure you have node js installed on your machine. If you don't you can download it by following this link https://nodejs.org/en/

2- Go to the project root folder and run **npm install** command to install all dependencies.

3- Run **npm start** to launch the application.

node version: 14.17.6