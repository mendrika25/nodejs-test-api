const User = require('../models/user.model');

const USERS = [
    {
        username: 'Diane'
    },
    {
        username: 'Léa'
    },
    {
        username: 'Eric'
    },
    {
        username: 'Jacob'
    }
]

const launchScript = async () => {
    const users = await User.find({});
    if (users.length === 0) {
        await User.insertMany(USERS, (err, res) => {
            console.log(`insert fake users successfully`);
        })
    }
  
}

module.exports.launchScript = launchScript;