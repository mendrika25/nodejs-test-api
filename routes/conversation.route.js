const router = require('express').Router();
const Conversation = require('../models/conversation.model');

router.post('/new', async (req, res) => {
    try {
        const newConversation = new Conversation({
            name: req.body.name,
            createdBy: req.body.createdBy,
            members: req.body.members
        });
        const conversation = await newConversation.save();
        res.status(200).json(conversation);
    } catch (error) {
        res.json(error);
    }
});

router.put('/close/:id', async (req, res) => {
    try {
        req.body.closed = true;
        await Conversation.findByIdAndUpdate(req.params.id, {$set: req.body});
        const conversation = await Conversation.findById(req.params.id);
        res.status(200).json(conversation);
    } catch (error) {
        res.json(error);
    }
});

router.put('/addMessage/:id', async (req, res) => {
    try {
        const message = {
            from: req.body.from,
            message: req.body.message
        }
        await Conversation.findByIdAndUpdate(req.params.id, {$push : {messages: message}});
        const conversation = await Conversation.findById(req.params.id).populate({path: 'messages', populate: {path: 'from'}});
        res.status(200).json(conversation);
    } catch (error) {
        res.json(error);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const conversation = await Conversation.findOne({_id: req.params.id}).populate({path: 'messages', populate: {path: 'from'}});
        res.status(200).json(conversation);
    } catch (error) {
        res.json(error);
    }
});

router.get('', async (req, res) => {
    try {
        const conversations = await Conversation.find({}).populate('createdBy members');
        res.status(200).json(conversations);
        
    } catch (error) {
        res.json(error);
    }
})

module.exports = router;
