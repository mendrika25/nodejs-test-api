const router = require('express').Router();
const User = require('../models/user.model');
const Conversation = require('../models/conversation.model');

router.get('', async(req, res) => {
    try {
        const users = await User.find({});
        res.status(200).json(users);
    } catch (error) {
        res.json(error);
    }
});

router.get('/:id', async(req, res) => {
    try {
        const userConversation =  await Conversation.find({$or : [{members : {$in: [req.params.id]}}, {createdBy: req.params.id}]}).populate('createdBy members');

        res.status(200).json(userConversation);
    } catch (error) {
        res.json(error);
    }
})

module.exports = router;